package net.pl3x.android.doublepunchslot;

public class Slot {
    private final Dimension width;
    private final Dimension length;

    public Slot(int x, int y) {
        this(new Dimension(x), new Dimension(y));
    }

    public Slot(Dimension dimension1, Dimension dimension2) {
        width = Dimension.min(dimension1, dimension2);
        length = Dimension.max(dimension1, dimension2);
    }

    public Dimension getWidth() {
        return width;
    }

    public Dimension getLength() {
        return length;
    }

    public Dimension getOffset() {
        int l = getLength().get32nds();
        int w = getWidth().get32nds();

        int o = ((l - (w * 2)) / 2) + (w / 2);

        // we don't want odd numbered 32nds.
        // add 1 to make it odd so it simplifies to 16ths
        if (o % 2 != 0) {
            o++;
        }

        return new Dimension(o);
    }
}
