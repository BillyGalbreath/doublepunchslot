package net.pl3x.android.doublepunchslot;

public class Dimension {
    private final int thirtySeconds;
    private final String fraction;

    public Dimension(int thirtySeconds) {
        this.thirtySeconds = thirtySeconds;
        this.fraction = from32nds();
    }

    public int get32nds() {
        return thirtySeconds;
    }

    public String getFraction() {
        return fraction;
    }

    public static Dimension min(Dimension dimension1, Dimension dimension2) {
        return (dimension1.get32nds() <= dimension2.get32nds()) ? dimension1 : dimension2;
    }

    public static Dimension max(Dimension dimension1, Dimension dimension2) {
        return (dimension1.get32nds() >= dimension2.get32nds()) ? dimension1 : dimension2;
    }

    private String from32nds() {
        int y = 32; // thirty seconds
        int gcd = gcd(thirtySeconds, y);

        if (thirtySeconds > y) {
            int a = thirtySeconds / y;
            int b = thirtySeconds - (a * y);
            String f = gcd >= 1 ? simplifyFraction(b, y, gcd) : b + "/" + y;
            return (a > 12 ? a / 12 + "-" + a % 12 : a) + (f.equals("") ? "" : "*" + f);
        }
        return gcd >= 1 ? simplifyFraction(thirtySeconds, y, gcd) : thirtySeconds + "/" + y;
    }

    private int gcd(int x, int y) {
        if (y == 0) {
            return x;
        }
        return gcd(y, x % y);
    }

    private String simplifyFraction(int x, int y, int gcd) {
        x = x / gcd;
        y = y / gcd;
        if (x == y) {
            return String.valueOf(x);
        }
        return x == 0 ? "" : x + "/" + y;
    }
}
