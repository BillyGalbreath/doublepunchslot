package net.pl3x.android.doublepunchslot;

import android.app.Activity;
import android.content.Context;
import android.inputmethodservice.KeyboardView;
import android.inputmethodservice.KeyboardView.OnKeyboardActionListener;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.InputType;
import android.view.HapticFeedbackConstants;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

class Keyboard {
    private final KeyboardView mKeyboardView;
    private final Activity mHostActivity;

    private final static int BACKSPACE = -1;
    private final static int ENTER = -2;
    private final static int ASTERISK = -3;
    private final static int SLASH = -4;
    private final static int DASH = -5;

    public Keyboard(Activity host, int viewid, int layoutid) {
        mHostActivity = host;

        mKeyboardView = (KeyboardView) mHostActivity.findViewById(viewid);

        mKeyboardView.setKeyboard(new android.inputmethodservice.Keyboard(mHostActivity, layoutid));
        mKeyboardView.setPreviewEnabled(false);
        mKeyboardView.setHapticFeedbackEnabled(true);

        mKeyboardView.setOnKeyboardActionListener(new OnKeyboardActionListener() {
            @Override
            public void onKey(int primaryCode, int[] keyCodes) {
                View focusCurrent = mHostActivity.getWindow().getCurrentFocus();

                if (focusCurrent == null || focusCurrent.getClass() != AppCompatEditText.class) {
                    return;
                }

                EditText edittext = (EditText) focusCurrent;
                Editable editable = edittext.getText();

                int start = edittext.getSelectionStart();
                int end = edittext.getSelectionEnd();
                if (end > start) {
                    editable.delete(start, end);
                }

                if (primaryCode == ENTER) {

                    // ##########

                    Data data = new Data(
                            mHostActivity,
                            ((EditText) mHostActivity.findViewById(R.id.etSlotDimension)).getText().toString(),
                            ((EditText) mHostActivity.findViewById(R.id.etCenterPoint)).getText().toString()
                    );

                    String result = data.checkSlot();
                    if (result != null) {
                        MainActivity.toast(mHostActivity, result);
                        return;
                    }

                    result = data.checkCenter();
                    if (result != null) {
                        MainActivity.toast(mHostActivity, result);
                        return;
                    }

                    Slot slot = data.getSlot();

                    Dimension slotWidth = slot.getWidth();
                    Dimension slotLength = slot.getLength();
                    Dimension slotOffset = slot.getOffset();
                    Dimension centerPoint = data.getCenter();
                    Dimension pitch = new Dimension(slot.getOffset().get32nds() * 2);

                    MainActivity.log("########################################################################");
                    MainActivity.log("Slot Width: " + slotWidth.getFraction() + " (" + slotWidth.get32nds() + "/32)");
                    MainActivity.log("Slot Length: " + slotLength.getFraction() + " (" + slotLength.get32nds() + "/32)");
                    MainActivity.log("Slot Offset: " + slotOffset.getFraction() + " (" + slotOffset.get32nds() + "/32)");
                    MainActivity.log("Center Point: " + centerPoint.getFraction() + " (" + centerPoint.get32nds() + "/32)");

                    Dimension slotMin = new Dimension(centerPoint.get32nds() - slotOffset.get32nds());
                    Dimension slotMax = new Dimension(centerPoint.get32nds() + slotOffset.get32nds());

                    MainActivity.log("Slot Min: " + slotMin.getFraction() + " (" + slotMin.get32nds() + "/32)");
                    MainActivity.log("Slot Max: " + slotMax.getFraction() + " (" + slotMax.get32nds() + "/32)");

                    ((TextView) mHostActivity.findViewById(R.id.tSlotResultMin)).setText(slotMin.getFraction());
                    ((TextView) mHostActivity.findViewById(R.id.tCenterResult)).setText(centerPoint.getFraction());
                    ((TextView) mHostActivity.findViewById(R.id.tSlotResultMax)).setText(slotMax.getFraction());
                    ((TextView) mHostActivity.findViewById(R.id.tPitch)).setText(pitch.getFraction());

                    //

                    RelativeLayout resultImage = (RelativeLayout) mHostActivity.findViewById(R.id.resultsImageHolder);
                    resultImage.removeAllViews();
                    resultImage.addView(new SlotImage(mHostActivity, slot));
                    //

                    focusCurrent.clearFocus();
                    hideCustomKeyboard();

                    // ###########

                } else if (primaryCode == BACKSPACE) {
                    if (editable != null && start > 0) editable.delete(start - 1, start);
                } else if (primaryCode == ASTERISK) {
                    editable.insert(start, "*");
                } else if (primaryCode == DASH) {
                    editable.insert(start, "-");
                } else if (primaryCode == SLASH) {
                    editable.insert(start, "/");
                } else {// Insert character
                    editable.insert(start, Character.toString((char) primaryCode));
                }
            }

            @Override
            public void onPress(int arg0) {
                if (arg0 != 0) {
                    mKeyboardView.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY);
                }
            }

            @Override
            public void onRelease(int primaryCode) {
            }

            @Override
            public void onText(CharSequence text) {
            }

            @Override
            public void swipeDown() {
            }

            @Override
            public void swipeLeft() {
            }

            @Override
            public void swipeRight() {
            }

            @Override
            public void swipeUp() {
            }
        });

        mHostActivity.findViewById(R.id.content).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideCustomKeyboard();
                return false;
            }
        });

        mHostActivity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    public void registerEditText(int id) {
        EditText edittext = (EditText) mHostActivity.findViewById(id);

        edittext.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    showCustomKeyboard(v);
                    return;
                }
                hideCustomKeyboard();
            }
        });

        edittext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showCustomKeyboard(v);
            }
        });

        edittext.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.onTouchEvent(event);
                InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null) {
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                return true;
            }
        });

        // Disable spell check (hex strings look like words to Android)
        edittext.setInputType(edittext.getInputType() | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
    }

    public void hideCustomKeyboard() {
        View focus = mHostActivity.getCurrentFocus();
        if (focus != null) {
            focus.clearFocus();
        }
        mKeyboardView.setVisibility(View.GONE);
        mKeyboardView.setEnabled(false);
    }

    public void showCustomKeyboard(View v) {
        mKeyboardView.setVisibility(View.VISIBLE);
        mKeyboardView.setEnabled(true);
        ((InputMethodManager) mHostActivity.getSystemService(Activity.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public boolean isCustomKeyboardVisible() {
        return mKeyboardView.getVisibility() == View.VISIBLE;
    }
}
