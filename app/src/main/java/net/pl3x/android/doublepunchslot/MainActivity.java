package net.pl3x.android.doublepunchslot;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.widget.Toast;

import java.util.logging.Logger;

public class MainActivity extends AppCompatActivity {
    private Keyboard keyboard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        keyboard = new Keyboard(this, R.id.keyboardview, R.xml.keyboard);

        keyboard.registerEditText(R.id.etSlotDimension);
        keyboard.registerEditText(R.id.etCenterPoint);
    }

    @Override
    public void onBackPressed() {
        if (keyboard.isCustomKeyboardVisible()) {
            keyboard.hideCustomKeyboard();
            return;
        }
        finish();
    }

    public static void log(String str) {
        Logger.getLogger("DoublePunchSlot").info(str);
    }

    public static void toast(Activity host, String message) {
        Toast toast = Toast.makeText(host, message, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.TOP | Gravity.CENTER, 0, 0);
        toast.show();
    }
}
