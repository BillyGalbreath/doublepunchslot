package net.pl3x.android.doublepunchslot;

import android.app.Activity;

public class Data {
    private final Activity host;
    private final String slotStr;
    private final String centerStr;

    private Slot slot;
    private Dimension center;

    public Data(Activity host, String slot, String center) {
        this.host = host;
        slotStr = slot.toLowerCase();
        centerStr = center.toLowerCase();
    }

    public Dimension getCenter() {
        return center;
    }

    public Slot getSlot() {
        return slot;
    }

    public String checkSlot() {
        String[] parts = slotStr.split("x");

        if (parts.length != 2) {
            return host.getString(R.string.slot_dimension_invalid);
        }

        int x = get32nds(parts[0]);
        int y = get32nds(parts[1]);

        if (x < 0 || y < 0) {
            return host.getString(R.string.slot_dimension_invalid);
        }

        slot = new Slot(x, y);

        return null; // no error
    }

    public String checkCenter() {
        int c = get32nds(centerStr);

        if (c < 0) {
            return host.getString(R.string.center_dimension_invalid);
        }

        center = new Dimension(c);

        return null; // no error
    }

    private int get32nds(String value) {
        if (value == null || value.equals("")) {
            return -1;
        }

        double inches = 0;
        String remaining = value;
        String[] splt;

        try {
            if (remaining.contains("-")) {
                if (remaining.indexOf("-") != remaining.lastIndexOf("-")) {
                    return -1;
                }
                splt = remaining.split("-");
                inches += 12 * Double.valueOf(splt[0]);
                remaining = splt[1];
            }

            if (remaining.contains("*")) {
                if (remaining.indexOf("*") != remaining.lastIndexOf("*")) {
                    return -1;
                }
                splt = remaining.split("\\*");
                inches += Double.valueOf(splt[0]);
                remaining = splt[1];

                if (remaining.contains("/")) {
                    if (remaining.indexOf("/") != remaining.lastIndexOf("/")) {
                        return -1;
                    }
                    splt = remaining.split("/");
                    inches += Double.valueOf(splt[0]) / Double.valueOf(splt[1]);
                    remaining = "";
                }
            }

            if (remaining.contains("/")) {
                if (remaining.indexOf("/") != remaining.lastIndexOf("/")) {
                    return -1;
                }
                splt = remaining.split("/");
                inches += Double.valueOf(splt[0]) / Double.valueOf(splt[1]);
                remaining = "";
            }

            if (!remaining.equals("")) {
                inches += Double.valueOf(remaining);
            }
        } catch (NumberFormatException e) {
            return -1;
        }

        return (int) (inches * 32);
    }
}
