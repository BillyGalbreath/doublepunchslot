package net.pl3x.android.doublepunchslot;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.view.View;

public class SlotImage extends View {
    private final Paint paintBlack;
    private final Paint paintWhite;
    private final int x, y;
    private int length, radius;
    private final Path pathTop, pathBottom;

    public SlotImage(Activity host, Slot slot) {
        super(host);

        paintBlack = new Paint();
        paintBlack.setColor(Color.BLACK);
        paintBlack.setAntiAlias(true);
        paintBlack.setStyle(Paint.Style.FILL);

        paintWhite = new Paint();
        paintWhite.setColor(Color.WHITE);
        paintWhite.setAntiAlias(true);
        paintWhite.setPathEffect(new DashPathEffect(new float[]{10, 10}, 0));
        paintWhite.setStyle(Paint.Style.STROKE);
        paintWhite.setStrokeWidth(3);

        int l = slot.getLength().get32nds();
        int w = slot.getWidth().get32nds();
        int imageWidth = host.findViewById(R.id.resultsImageHolder).getWidth();
        int imageHeight = host.findViewById(R.id.resultsImageHolder).getHeight();

        try {
            int multiplier = imageWidth / l;

            MainActivity.log(multiplier + "");

            if (w * multiplier >= imageHeight) {
                multiplier = imageHeight / w;
            }

            radius = w * multiplier / 2;
            length = (l - w) * multiplier;
        } catch (ArithmeticException e) {
            e.printStackTrace();
        }

        x = (imageWidth / 2) - (length / 2);
        y = radius;

        pathTop = new Path();
        pathTop.moveTo(x, y - radius + 1);
        pathTop.quadTo(x, y - radius + 1, x + length, y - radius + 1);

        pathBottom = new Path();
        pathBottom.moveTo(x, y + radius + 1);
        pathBottom.quadTo(x, y + radius + 1, x + length, y + radius + 1);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        for (int i = 0; i <= length; i++) {
            canvas.drawCircle(x + i, y, radius, paintBlack);
        }

        canvas.drawCircle(x, y + 1, radius, paintWhite);
        canvas.drawCircle(x + length, y + 1, radius, paintWhite);

        canvas.drawPath(pathTop, paintWhite);
        canvas.drawPath(pathBottom, paintWhite);
    }

}
